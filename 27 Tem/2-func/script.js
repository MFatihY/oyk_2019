function foo1(param) {
	return param;
}

var foo2 = function (param) {
	return param;
}

var foo3 = (param) => {
	return param;
}

if (foo1(true) && foo2(true) && foo3(true)) {
	console.log("All 3 function definition types work the same.");
}
