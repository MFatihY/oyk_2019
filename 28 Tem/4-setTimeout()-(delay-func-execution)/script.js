var someFunction = function () {
	console.log("This message is delayed.");
}

var someNumberInMiliseconds = 2000;

setTimeout(someFunction, someNumberInMiliseconds);
