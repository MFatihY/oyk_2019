const posts = [
	{
		title: "Başlık1",
		body: "Lorem ipsums 1"
	},
	{
		title: "Başlık2",
		body: "Lorem ipsum 2"
	}
];

function getPosts() {
	console.log(posts);
}

// These lines below cause getPosts() not to show the last added post because it logs before the last post is added.

function createPost(post) {
	setTimeout(() => {
		posts.push(post);
	}, 5000);
}

createPost({title: "Başlık3", body: "Lorem ipsum 3"});
getPosts();

// To fix this issue, we can send getPosts() as a callback function to createPostFixed(post, callback)

function createPostFixed(post, callback) {
	setTimeout(() => {
		posts.push(post);
		callback();
	}, 5000);
}

createPostFixed({title: "Başlık4", body: "Lorem ipsum 4"}, getPosts);

