const posts = [
	{
		title: "Başlık1",
		body: "Lorem ipsums 1"
	},
	{
		title: "Başlık2",
		body: "Lorem ipsum 2"
	}
];

function getPosts() {
	console.log(posts);
}

function updatePost(postIndex, postProperty, value) {
	posts[postIndex][postProperty] = value;
}

function updatePostWithoutArguments() {
	posts[0].body = "Başka bir body";
}

function createPostFixed(post, callback, callback2, callback3, updatePostIndex, updatePostProperty, updateValue) {
	setTimeout(() => {
		posts.push(post);
		callback2();
		callback3(updatePostIndex, updatePostProperty, updateValue);
		callback();
	}, 5000);
}

createPostFixed({title: "Başlık4", body: "Lorem ipsum 4"}, getPosts, updatePostWithoutArguments, updatePost, 0, "title", "Başka bir başlık");

