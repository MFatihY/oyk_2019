var arr = [1, 2, 3, 4, 6];

arr.forEach((currentElement, currentIndex) => {
	console.log("Element in index", currentIndex, "is", currentElement);
});
