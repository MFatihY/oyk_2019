var myObject = {
	id: 1,
	name: "mehmet",
	objects: [
		{
			numbers: [1, 2, 3, 4, 5],
			strings: ["a", "b", "c", "d", "e"]
		},
		{
			numbers: [5, 4, 3, 2, 1],
			strings: ["z", "y", "x", "w", "v"]
		}
	]
};

for (let key in myObject) {
	if (typeof(myObject[key]) != "object") {
		//~ console.log("if: ", typeof(myObject[key]))
		console.log(key + ": ", myObject[key]);
	}
	else {
		console.log(key)
		for (let index in myObject[key]) {
			for (let key_inside in myObject[key][index]) {
				for (let index_inside in myObject[key][index][key_inside])
				console.log(index_inside + ": " , myObject[key][index][key_inside][index_inside])
			}
		}
	}
}

//~ arr.forEach((currentElement, currentIndex) => {
	//~ console.log("Element in index", currentIndex, "is", currentElement);
//~ });
