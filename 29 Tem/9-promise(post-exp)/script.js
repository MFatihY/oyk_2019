const posts = [
	{
		title: "Başlık1",
		body: "Lorem ipsums 1"
	},
	{
		title: "Başlık2",
		body: "Lorem ipsum 2"
	}
];

function getPosts() {
	console.log(posts);
}

// These lines below cause getPosts() not to show the last added post because it logs before the last post is added.

function createPost(post) {
	setTimeout(() => {
		posts.push(post);
	}, 5000);
}

createPost({title: "Başlık3", body: "Lorem ipsum 3"});
getPosts();

// To fix this issue, we sent before, getPosts() as a callback function to createPostFixed(post, callback)
// To fix this issue, we could also use a Promise object as in below

function createPostFixed(post) {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			posts.push(post);
			const error = false; //or true, try it
			if (!error)
				resolve("Success!"); // message is optional
			else 
				reject("Failure! An error occured."); // message is optional
		}, 5000);
	})
}

createPostFixed({title: "Başlık4", body: "Lorem ipsum 4"}).then((response) => {
	console.log(response);
	getPosts();
}).catch((e) => {
	console.log(e);
});

