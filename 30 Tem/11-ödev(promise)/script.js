/* ödev metninde geçen "objelerdeki bu görevlerin kime ait olduğunu arraya atacak
 * daha sonra ise tek tek string olarak consola yazacak" ifadesini tam anlamadığım
 * için .then() içinde sadece array'i bastırdım.
*/

var todos = [
	{
		id: 2,
		explanation: "açıklama 2",
		title: "Başlık 2",
		todo: ["yemek", "yatmak", "çalışmak"]
	},
	
	{
		id: 3,
		explanation: "açıklama 3",
		title: "Başlık 3",
		todo: ["içmek", "kalkmak", "çalışmamak"]
	},
	{
		id: 1,
		explanation: "açıklama 1",
		title: "Başlık 1",
		todo: ["yürümek", "koşmak", "bu ödev"]
	}
];

function sortTodosById(todos) {
	var tmp;
	for (let i = 0; i < todos.length-1; i++)
		for (let j = 0; j < todos.length-1; j++)
			if (todos[j].id > todos[j+1].id) {
				tmp = todos[j];
				todos[j] = todos[j+1];
				todos[j+1] = tmp;
			}
}

function addMultipleTodos(todosDest, todosSrc) {
	for (let i = 0; i < todosSrc.length; i++)
		todosDest.push(todosSrc[i]);
}

var myPromise = new Promise((resolve, reject) => {
	setTimeout(() => {
		var error = false; // would become true afterwards if an error occured but there is not such situation.
		sortTodosById(todos);
		addMultipleTodos(todos, [
			{
				id: 4,
				explanation: "açıklama 4",
				title: "Başlık 4",
				todo: ["farklı 1 şey", "farklı başka 1 şey", "farklı daha da başka 1 şey"]
			},
			{
				id: 5,
				explanation: "açıklama 5",
				title: "Başlık 5",
				todo: ["daha farklı 1 şey", "daha farklı başka 1 şey", "daha farklı daha da başka 1 şey"]
			},
			{
				id: 6,
				explanation: "açıklama 6",
				title: "Başlık 6",
				todo: ["daha da farklı 1 şey", "daha da farklı başka 1 şey", "daha da farklı daha da başka 1 şey"]
			}
		]);
		
		todos.pop(); // removes from end
		todos.shift(); // removes from beginning
		
		for (let i = 0; i < todos.length; i++)
			todos[i].someOtherKey = "someOtherValue " + (todos[i].id);
		
		if (!error) resolve();
		else reject();
	}, 4000);
});

myPromise.then(() => {
	console.log(todos);
}).catch(() => {
	console.log("Sorry. An error occured.");
});

console.log(todos); // shows the first state of todos because this line runs while myPromise is waiting for 3.5 seconds
