axios.get("https://jsonplaceholder.typicode.com/todos")
	.then((response) => {
		console.log(response.data)
		let data = response.data
		data.forEach((item) => {console.log(item.title)})
	})
	.catch((error) => {console.log(error)})

axios.get("https://jsonplaceholder.typicode.com/posts")
	.then((response) => {
		console.log(response)
		let data = response.data
		data.forEach((item, index) => {
			console.log(item.body)
		})
	})
