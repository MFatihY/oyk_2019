var ar = [1, 2, 3]
var obj = {key1: "a", key2: "b"}

console.log("for..in for object")
for (let key in obj) {
	console.log(key)
	console.log(obj[key])
}

console.log("\nfor..in for array")
for (let index in ar) {
	console.log(index)
	console.log(ar[index])
}

console.log("\nfor..of for array")
for (let item of ar) console.log(item)

console.log("\nfor..of for object (doesn't work)")
for (let index of obj) ; // gives TypeError
