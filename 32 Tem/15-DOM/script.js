//~ window.alert("This is an alert!")

//~ if (confirm("Are you sure?")) console.log("Yes")
//~ else console.log("Nope")

//~ var input = prompt()

//~ console.log(location.href)

//~ console.log(document.getElementById("title"))
//~ console.log(document.getElementById("title").id)
//~ console.log(document.getElementById("title").className)
//~ console.log(document.getElementsByClassName("myclass"))
//~ console.log(document.getElementsByName("title-name"))
//~ console.log(document.getElementsByTagName("h1"))

//~ console.log(document.querySelector("h1"))
//~ console.log(document.querySelectorAll("h1"))
//~ console.log(document.querySelectorAll(".myclass"))
//~ console.log(document.querySelectorAll("#title"))

var title = document.querySelector("#title")
console.log(title)
title.style.background = "red"
title.style.color = "white"
title.textContent = "Başlık2" // how are textContent and
title.innerText = "Başlık3" //   innerText different?
title.innerHTML = "<em>Başlık4</em>"
// So, innerText also contains CSS properties.
// Therefore, it's the costliest one among these 3.
