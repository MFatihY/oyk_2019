var spinner = document.getElementById("spinner")

axios.get("https://jsonplaceholder.typicode.com/todos")
	.then((response) => {
		console.log(response)
		let data = response.data
		console.log(data)
		
		let output = ""
		data.forEach((item) => {
			output += `
				<div>
					<h1>${item.title}</h1>
					<h3>Item ID: ${item.id} User ID: ${item.userId}</h3>
					<h5>${item.completed ? "Yes" : "No"}</h5>
					
				</div>
			`
			//~ console.log(output)
		})
		setTimeout(() => {
			spinner.style.display = "none"
			document.getElementById("post-container").innerHTML = output
		}, 2500)
	})
