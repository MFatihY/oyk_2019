var todoList = [
	{
		id: 1,
		done: true
	},
	{
		id: 2,
		done: false
	},
	{
		id: 3,
		done: true
	},
	{
		id: 4,
		done: false
	},
	{
		id: 5,
		done: false
	},
	{
		id: 6,
		done: true
	},
	{
		id: 7,
		done: true
	},
	{
		id: 8,
		done: true
	},
	{
		id: 9,
		done: false
	},
	{
		id: 10,
		done: false
	},
]

const ulDone =  document.createElement("ul")
const ulUndone =  document.createElement("ul")
var li = {}

for (let i = 0; i < todoList.length; i++) {
	li = document.createElement("li")
	li.appendChild(document.createTextNode(todoList[i].id))
	if (todoList[i].done) ulDone.appendChild(li)
	else ulUndone.appendChild(li)
}

const body = document.querySelector("body")

const divDone = document.createElement("div")
divDone.className = "todo-list"

const divUndone = document.createElement("div")
divUndone.className = "todo-list"

const titleDone = document.createElement("h2")
titleDone.appendChild(document.createTextNode("DONE"))

const titleUndone = document.createElement("h2")
titleUndone.appendChild(document.createTextNode("UNDONE"))

divDone.appendChild(titleDone)
divDone.appendChild(ulDone)

divUndone.appendChild(titleUndone)
divUndone.appendChild(ulUndone)

body.appendChild(divDone)
body.appendChild(divUndone)

