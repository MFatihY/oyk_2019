var todoList = [
	{
		id: 1,
		done: true
	},
	{
		id: 2,
		done: false
	},
	{
		id: 3,
		done: true
	},
	{
		id: 4,
		done: false
	},
	{
		id: 5,
		done: false
	},
	{
		id: 6,
		done: true
	}
]

const body = document.querySelector("body")

const divDone = document.createElement("div")
divDone.className = "todo-list"

const divUndone = document.createElement("div")
divUndone.className = "todo-list"

const titleDone = document.createElement("h2")
titleDone.appendChild(document.createTextNode("DONE"))

const titleUndone = document.createElement("h2")
titleUndone.appendChild(document.createTextNode("UNDONE"))

const ulDone =  document.createElement("ul")
const ulUndone =  document.createElement("ul")

divDone.appendChild(titleDone)
divDone.appendChild(ulDone)

divUndone.appendChild(titleUndone)
divUndone.appendChild(ulUndone)

body.appendChild(divDone)
body.appendChild(divUndone)

var li = {}
for (let i = 0; i < todoList.length; i++) {
	li = document.createElement("li")
	li.appendChild(document.createTextNode(todoList[i].id))
	if (todoList[i].done) ulDone.appendChild(li)
	else ulUndone.appendChild(li)
}

var inputs = document.querySelectorAll("input")

var radioInput

if (inputs[2].checked) radioInput = true
if (inputs[3].checked) radioInput = false


var submit = document.querySelector("button")

submit.addEventListener("click", () => {
	console.log(radioInput)
	todoList.push({todo: inputs[0].value, id: inputs[1].value, done: radioInput})
	li = document.createElement("li")
	li.appendChild(document.createTextNode(todoList[todoList.length-1].id))
	if (todoList[todoList.length-1].done) ulDone.appendChild(li)
	else ulUndone.appendChild(li)
})

console.log(todoList)






