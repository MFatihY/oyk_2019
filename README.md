Bu depo (repository), Linux Kullanıcıları Derneği (https://www.lkd.org.tr) tarafından düzenlenen Özgür Yazılım Yaz Kampı 2019'da Web Önyüz (Front-end) Programlama kursunda yazılan kaynak kodlarını içerir.  
Belirtmeliyim ki buradaki bütün kodlar tarafımdan yazılmamıştır, birazı eğitmenler tarafından yazılmıştır.


ENGLISH:  
This is a repository that includes source code written at Free Software Summer Camp 2019 organized by Turkish Linux Users Association (https://www.lkd.org.tr/en)  
Note that not all source code here is written by me, but some of it by instructors.  
Sorry for some Turkish content in this repository.